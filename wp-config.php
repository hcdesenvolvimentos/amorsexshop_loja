<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_amorsexshop_loja' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(98,X5gwd]G&G7]Y^7#cjF8/@#emo==I(!zDvrN4xnRZ j/L;wONM`B=L$R(j&~p' );
define( 'SECURE_AUTH_KEY',  ':OSFE`1sgU6A}?dD;o[4n^>5N&^cvm<lI>M<[iB}Jo[e^a}?l(eQO+BE{S^qzI(}' );
define( 'LOGGED_IN_KEY',    '+ MQB?-&bD#x)qP=LOE|0j8~ky;M}4rF2)QLzAPC_kUz)76Je}U7:mPD!)28Gg}D' );
define( 'NONCE_KEY',        '(SLK)zN2Zo]j,9(dqGq3lOi;8{o4ghUr~qZ5,s=_|U/vnKy/Hvbin O# m6>tsk.' );
define( 'AUTH_SALT',        'O0nGpj#T.{<rbzD0:mr&.`%8/YLMRT=#{[ ww<5a^W$pSlx2!{a?.e/JxI>h(^.H' );
define( 'SECURE_AUTH_SALT', 'Ep*K:ixB)[gnET:`_Z~XnPszcG(;YZ p&O6G%OGQtFn661<l[zVbbQ*ax6Z,y8Xz' );
define( 'LOGGED_IN_SALT',   ' 3)f.O;%RMjN(E~CjyBP0Tu7QYS2d59H^:9BMQydP4cHjn#Yk_Z?EqhV;kNWn}DG' );
define( 'NONCE_SALT',       '0e]1!jc5Z}wYc3mJx$>qM,Z#8#oemOGS`nD7t}>os)@y3:&,.Ak*Y&,PU_1rfUC,' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'as_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
