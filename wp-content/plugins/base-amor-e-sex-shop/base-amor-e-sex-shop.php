<?php

/**
 * Plugin Name: Base Amor e Sex Shop
 * Description: Controle base do tema Amor e Sex Shop.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */

	function baseAmoreSexShop(){
		// TIPOS DE CONTEÚDO
		conteudosAmoreSexShop();

		metaboxesAmoreSexShop();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosAmoreSexShop(){

		// TIPOS DE CONTEÚDO
		tipoDestaque();

		// tipoMiniBannerDestaque();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
							
				default:
				break;
			}

		    return $titulo;

		}

    }
    // CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosdestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsdestaque 	= array(
								'labels'             => $rotulosdestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaques' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsdestaque);

	}
	// CUSTOM POST TYPE MINIS DESTAQUES
	function tipoMiniBannerDestaque() {

		$rotulosminibannerdestaque = array(
								'name'               => 'Mini banners Destaques',
								'singular_name'      => 'Mini banner destaque',
								'menu_name'          => 'Mini banners de Destaques',
								'name_admin_bar'     => 'Mini banners de Destaques',
								'add_new'            => 'Adicionar novo mini banner',
								'add_new_item'       => 'Adicionar novo mini destaque',
								'new_item'           => 'Novo mini banner destaque',
								'edit_item'          => 'Editar mini banner destaque',
								'view_item'          => 'Ver mini banner destaque',
								'all_items'          => 'Todos os mini banners destaques',
								'search_items'       => 'Buscar mini banners destaque',
								'parent_item_colon'  => 'Dos mini banners destaques',
								'not_found'          => 'Nenhum mini banner destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum mini banner destaque na lixeira.'
							);

		$argsminibannerdestaque 	= array(
								'labels'             => $rotulosminibannerdestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'mini-banners-destaques' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('minibannerdestaque', $argsminibannerdestaque);

	}

/****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAmoreSexShop(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

	function registraMetaboxes( $metaboxes ){

			$prefix = 'AmoreSexShop_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link destaque: ',
						'id'    => "{$prefix}destaque_link",
						'desc'  => '',
						'type'  => 'text',
					),	
					
									
				),
			);

            // METABOX DE MINI DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxMiniBannerDestaqueInicial',
				'title'			=> 'Detalhes do Mini Banner Destaque',
				'pages' 		=> array( 'miniBannerDestaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link do Mini Banner Destaque: ',
						'id'    => "{$prefix}mini_banner_destaque_link",
						'desc'  => '',
						'type'  => 'text',
					),	
					
									
				),
			);	

		
		    // METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxProduto',
				'title'			=> 'Foto zoom produto',
				'pages' 		=> array( 'product' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto zoom produto: ',
						'id'    => "{$prefix}produto_zoom",
						'desc'  => '',
						'type'  		   => 'image_advanced',
                        'max_file_uploads' => 1
					),	
					
									
				),
			);

			return $metaboxes;
	}

	

/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesAmoreSexShop(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerAmoreSexShop(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAmoreSexShop');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseAmoreSexShop();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );



	
// ADICIONANDO CAMPOS PERSOMALIZADOS DE EDIÇÃO PÁGINA DE CATEGORIA DO PRODUTO - WOOCOMERCE
function product_cat_add_meta_fields( $taxonomy ) {
    ?>
    <div class="form-field term-group">
        <label for="img_cat_menu">Imagem Categoria Menu</label>
        <input type="text" id="img_cat_menu" name="img_cat_menu" />
        <label for="img_cat_link">Url link Categoria Menu</label>
        <input type="text" id="img_cat_link" name="img_cat_link" />
    </div>
    <?php
}
add_action( 'product_cat_add_form_fields', 'product_cat_add_meta_fields', 10, 2 );

// ADICIONANDO CAMPOS PERSOMALIZADOS DE EDIÇÃO DENTRO PÁGINA DE CATEGORIA DO PRODUTO - WOOCOMERCE
function product_cat_edit_meta_fields( $term, $taxonomy ) {
    $img_cat_menu = get_term_meta( $term->term_id, 'img_cat_menu', true );
    $img_cat_link = get_term_meta( $term->term_id, 'img_cat_link', true );
    ?>
    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="img_cat_menu">Imagem Categoria Menu</label>
        </th>
        <td>
            <input type="text" id="img_cat_menu" name="img_cat_menu" value="<?php echo $img_cat_menu; ?>" />
        </td>
    </tr>
    <tr class="form-field term-group-wrap">
        <th scope="row">
            <label for="img_cat_link">Url link Categoria Menu</label>
        </th>
        <td>
            <input type="text" id="img_cat_link" name="img_cat_link" value="<?php echo $img_cat_link; ?>" />
        </td>
    </tr>
    <?php
}
add_action( 'product_cat_edit_form_fields', 'product_cat_edit_meta_fields', 10, 2 );

//REGISTRANDO CAMPOS PERSOMALIZADOS NO BANCO 
function product_cat_save_taxonomy_meta( $term_id, $tag_id ) {
    if( isset( $_POST['img_cat_menu'] ) ) {
        update_term_meta( $term_id, 'img_cat_menu', esc_attr( $_POST['img_cat_menu'] ) );
    }
    if( isset( $_POST['img_cat_link'] ) ) {
        update_term_meta( $term_id, 'img_cat_link', esc_attr( $_POST['img_cat_link'] ) );
    }
}
add_action( 'created_product_cat', 'product_cat_save_taxonomy_meta', 10, 2 );
add_action( 'edited_product_cat', 'product_cat_save_taxonomy_meta', 10, 2 );

