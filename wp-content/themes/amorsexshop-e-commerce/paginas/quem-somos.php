<?php

/**
* Template Name: Quem Somos
* Description: Página Quem Somos
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package amorsexshop-e-commerce
*/

get_header();
?>
<!-- PÁGINA QUEM SOMOS -->
<div class="pg pg-produto">
		<div class="container-full">
			<div class="row">
				<div class="borda-categorias">
					<div class="col-sm-3">
						<!-- SIDEBAR -->
						<?php
							//SIDEBAR
							include (TEMPLATEPATH . '/templates/sidebar_institucional.php');
						?>
					</div>
					<div class="col-sm-9">
						<div class="quem-somos">							
							<div class="contaudo-quem-somos">
								<div class="banner-titulo-quem-somos " style="background: url(<?php echo $configuracao['opt_banner_quem_somos']['url'] ?>);">
									<h3 class="titulo"><?php echo get_the_title(); ?></h3>
								</div>
								<div class="texto-quem-somos">
								<?php
									while(have_posts()){
										the_post(); 
										echo the_content();
									} 
								?>
								</div>
								<div class="nossas-vantagens">
									<h3 class="titulo">Nossas <strong>vantagens</strong></h3>
									<ul>
										<li>
											<figure class="icon">
												<img src="<?php echo $configuracao['opt_nossas_vantagens1_imagem']['url'] ?> " alt="Icone Sobre a Amor Sex Shop">
											</figure>
											<div class="texto-vantagem">
												<h6 class="titulo titulo-vantagem"><?php echo $configuracao['opt_nossas_vantagens1_titulo']; ?></h6>
												<p><?php echo $configuracao['opt_nossas_vantagens1_descricao']; ?></p>
											</div>
										</li>
										<li>
											<figure class="icon">
												<img src="<?php echo $configuracao['opt_nossas_vantagens2_imagem']['url'] ?> " alt="Icone Sobre a Amor Sex Shop">
											</figure>
											<div class="texto-vantagem">
												<h6 class="titulo titulo-vantagem"><?php echo $configuracao['opt_nossas_vantagens2_titulo']; ?></h6>
												<p><?php echo $configuracao['opt_nossas_vantagens2_descricao']; ?></p>
											</div>
										</li>
										<li>
											<figure class="icon">
												<img src="<?php echo $configuracao['opt_nossas_vantagens3_imagem']['url'] ?> " alt="Icone Sobre a Amor Sex Shop">
											</figure>
											<div class="texto-vantagem">
												<h6 class="titulo titulo-vantagem"><?php echo $configuracao['opt_nossas_vantagens3_titulo']; ?></h6>
												<p><?php echo $configuracao['opt_nossas_vantagens3_descricao']; ?></p>
											</div>
										</li>
										<li>
											<figure class="icon">
												<img src="<?php echo $configuracao['opt_nossas_vantagens4_imagem']['url'] ?> " alt="Icone Sobre a Amor Sex Shop">
											</figure>
											<div class="texto-vantagem">
												<h6 class="titulo titulo-vantagem"><?php echo $configuracao['opt_nossas_vantagens4_titulo']; ?></h6>
												<p><?php echo $configuracao['opt_nossas_vantagens4_descricao']; ?></p>
											</div>
										</li>
										
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>


<?php get_footer();