<?php

/**
* Template Name: Lista de desejos
* Description: Lista de desejo
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package amorsexshop-e-commerce
*/

get_header();
?>
<div class="container">
	<?php echo do_shortcode('[yith_wcwl_wishlist]'); ?>
</div>
<?php get_footer();