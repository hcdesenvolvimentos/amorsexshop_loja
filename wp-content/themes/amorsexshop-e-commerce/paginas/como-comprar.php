<?php

/**
* Template Name: Como Comprar
* Description: Página Como comprar
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package amorsexshop-e-commerce
*/

get_header();
?>

<!-- PÁGINA DE COMO COMPRAR -->
<div class="pg pg-como-comprar">
		<div class="container-full">
			<div class="row">
				<div class="borda-categorias">
					<div class="col-sm-3">
						<!-- SIDEBAR -->
					</div>
					<div class="col-sm-9">
						<div class="como-comprar">
							<nav class="menu-caminho">
								<a href="#">Amor Sex Shop</a>
								<a href="#">Lingeries</a>
								<a href="#">Espartilhos</a>
							</nav>
							<h5 class="titulo">Como comprar?</h5>
							<ul>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
<?php get_footer();