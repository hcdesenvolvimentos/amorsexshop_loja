<?php

/**
* Template Name: Inicial
* Description: Página Inicial
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package amorsexshop-e-commerce
*/

get_header();
?>
<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">
	
	<?php 
		// CARROSSEL DESTAQUE
		include (TEMPLATEPATH . '/templates/templates_home/carrossel_destaque.php');
	?>
		<div class="container-full">
			<?php 
				// BANNER FRETE PROMOCIONAL
				include (TEMPLATEPATH . '/templates/templates_home/banner_frete_promocional.php');

				// MINI BANNER DESTAQUE
				include (TEMPLATEPATH . '/templates/templates_home/mini_banner_destaque.php');
			 ?>	
			<!-- SEÇÃO PRODUTOS -->
			<section class="secao-produtos">
			<?php 
				// CARROSSEL PRODUTO DEPARTAMENTO 1 
				include (TEMPLATEPATH . '/templates/templates_home/carrossel_produto_departamento/carrossel_produto_1.php');

				// CARROSSEL  MARCA
				include (TEMPLATEPATH . '/templates/templates_home/carrossel_marcas.php');

				// CARROSSEL RODUTO DEPARTAMENTO 2
				include (TEMPLATEPATH . '/templates/templates_home/carrossel_produto_departamento/carrossel_produto_2.php');

				// CARROSSEL RODUTO DEPARTAMENTO 3
				include (TEMPLATEPATH . '/templates/templates_home/carrossel_produto_departamento/carrossel_produto_3.php');

				// CARROSSEL RODUTO DEPARTAMENTO 4
				include (TEMPLATEPATH . '/templates/templates_home/carrossel_produto_departamento/carrossel_produto_4.php');
		    ?>
							
				
			</section>
		</div>
</div>


<?php get_footer();