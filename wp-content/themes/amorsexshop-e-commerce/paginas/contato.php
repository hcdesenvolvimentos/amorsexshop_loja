<?php

/**
* Template Name: Contato
* Description: Página Contato
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package amorsexshop-e-commerce
*/

get_header();
?>
<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">
		<div class="container-full">
			<div class="row">
				<div class="col-sm-3">
					<!-- SIDEBAR -->
					<?php
						//SIDEBAR
						include (TEMPLATEPATH . '/templates/sidebar_institucional.php');
					?>
				</div>
				<div class="col-sm-9">
					<div class="fale-conosco">
						
						<div class="formulario">
							<h5 class="titulo"><?php echo get_the_title(); ?></h5>
							<!-- <form action=""> -->
								<!-- <label for="name">Seu nome(obrigatório)</label>
								<input type="text" id="name" placeholder="nome">
								<label for="email">Seu e-mail</label>
								<input type="email" id="email" placeholder="e-mail">
								<label for="city">Sua cidade</label>
								<input type="text" id="city" placeholder="cidade">
								<label for="phone">Seu telefone</label>
								<input type="tel" id="phone" placeholder="telefone">
								<label for="message">Sua mensagem</label>
								<textarea name="Message" id="message" cols="30" rows="10" placeholder="pode falar..."></textarea>
								<input type="submit"> -->
							<!-- </form> -->
							<?php echo do_shortcode('[contact-form-7 id="6" title="Fomulário de contato"]'); ?>

						</div>
					</div>
				</div>
			</div>
		</div>
</div>



<?php get_footer();