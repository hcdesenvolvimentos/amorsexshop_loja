<?php

/**
* Template Name: Institucional
* Description: Página Institucional
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package amorsexshop-e-commerce
*/

get_header();
?>
<!-- PÁGINA INSTITUCIONAL -->
	<div class="pg pg-institucional">
		<div class="container-full">
			<div class="row">
				<div class="borda-categorias">
					<div class="col-sm-3">
						<!-- SIDEBAR -->
					<?php
						//SIDEBAR
						include (TEMPLATEPATH . '/templates/sidebar_institucional.php');
					?>
					</div>
					<div class="col-sm-9">
						<div class="institucional">
							<nav class="menu-caminho">
								<a href="#">Amor Sex Shop</a>
								<a href="#"><?php echo get_the_title(); ?></a>
								
							</nav>
							<h2 class="titulo"><?php echo get_the_title();?></h2>
							<div class="conteudo">
								<?php
								while(have_posts()){
									the_post(); 
									echo the_content();
								} 
							?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer();