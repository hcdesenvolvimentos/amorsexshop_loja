$(function(){

	/*****************************************
	*           CARROSSEIS                   *
	*****************************************/

	$('.carrossel-destaque').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		dots: true,
		infinite: true,
		speed: 450,
		autoplay: true,
		autoplaySpeed: 3000,
		centerMode: true,
		pauseOnHover: true
	});

	$('.carrossel').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		fade: false,
		dots: false,
		infinite: false,
		speed: 450,
		autoplay: false,
		autoplaySpeed: 3000,
		centerMode: false,
		pauseOnHover: true,
		edgeFriction: 0,
		responsive: [
	       	{
	          breakpoint: 400,
	          settings: {
	            slidesToShow: 1,
	           
	          }
	        },
			{
	          breakpoint: 610,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 810,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 4,
	          }
	        }
	      ]
	});

	$('.carrossel-produto').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		fade: false,
		dots: false,
		infinite: true,
		speed: 450,
		autoplay: false,
		autoplaySpeed: 3000,
		centerMode: false,
		pauseOnHover: true,
		edgeFriction: 0
	});

	$('.pg-produto ul.carrossel-produto li.item-produto figure img').click(function(){
		$('.pg-produto figure.imagem-produto img').attr('src', $(this).attr('src'));
	});

	let isOpen = false;
	$('header .abrir-menu-mobile').click(function(){
		$('header .menu-principal').toggleClass('menu-mobile-ativo');

		if(!isOpen){
			$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').addClass('translate');
			$('header .abrir-menu-mobile span.dois').addClass('opacity-zero');
			setTimeout(function(){
				$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').addClass('rotate');
			},200);
			isOpen = true;
		} else{
			$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').removeClass('rotate');
			$('header .abrir-menu-mobile span.dois').removeClass('opacity-zero');
			setTimeout(function(){
				$('header .abrir-menu-mobile span.um,header .abrir-menu-mobile span.tres').removeClass('translate');
			},200);
			isOpen = false;
		}
	});

	$('.pg ul.lista-produtos li.item-produto a.imagem-produto figure').mouseover(function(){
		$(this).children().attr('src', $(this).children().attr('data-hover'));
	});

	$('.pg ul.lista-produtos li.item-produto a.imagem-produto figure').mouseout(function(){
		$(this).children().attr('src', $(this).children().attr('data-inicial'));
	});

	 jQuery('header .menu-principal ul li.menu-principal-item img').each(function(){
	    var $img = jQuery(this);
	    var imgID = $img.attr('id');
	    var imgClass = $img.attr('class');
	    var imgURL = $img.attr('src');

	    jQuery.get(imgURL, function(data) {
	       
			// Pega a tag SVG, ignora o resto
	        var $svg = jQuery(data).find('svg');

			// Adicionar o ID da imagem substituída ao novo SVG
	        if(typeof imgID !== 'undefined') {
	            $svg = $svg.attr('id', imgID);
	        }
	        
			// Adicionar as classes da imagem substituída ao novo SVG
	        if(typeof imgClass !== 'undefined') {
	            $svg = $svg.attr('class', imgClass+' replaced-svg');
	        }
	       
			// Remova qualquer tag XML inválida conforme http://validator.w3.org
	        $svg = $svg.removeAttr('xmlns:a');

			// Substituir imagem por novo SVG
	        $img.replaceWith($svg);

	    }, 'xml');

	});

	$(".btnAddCart").click(function(e){
		setTimeout(function(){ 
			updateMinicart();
		}, 2000);
	});
	$(".remove.remove_from_cart_button").click(function(e){
		setTimeout(function(){ 
			updateMinicart();
		}, 2000);
	});

	function updateMinicart() {
		var data = {
			'action': 'mode_theme_update_mini_cart'
		};
		$.post(
		   woocommerce_params.ajax_url, 
		   data, 
		   function(response){
		     $('#mini-cart-loja').html(response); 
		   }
		);
	}

	$(".add-lista-desejos").click(function(e){
		var id_product_wishlist = $(this).attr("data-add_to_wishlist");
		addWishlist(id_product_wishlist);
	});

	function addWishlist(id_product_wishlist) {
		jQuery.ajax({
		 	type: 'POST',
		 	url: 'http://loja.amorsexshop.com.br/?add_to_wishlist='+id_product_wishlist,
		 	data: { 
		 	
			 	'add_to_wishlist': id_product_wishlist,
			 	'product_type'   : 'simple',
			 	'action'         : 'add_to_wishlist',
			},
		 	success: function(response, textStatus, jqXHR){
           		
        	},
        
    	});
	}
		
});

