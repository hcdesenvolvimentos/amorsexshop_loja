<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amorsexshop-e-commerce
 */


global $configuracao;
$get_category_product   = 'product_cat';
$get_category_products = get_terms( $get_category_product, array(
		'orderby'       => "count",
		'hide_empty'    => "true",
		'order'         => "DESC",
		'parent'        => 0,
	));
?>


<footer>
		<!-- NEWSLETTER -->
		<div class="newsletter">
			<div class="container-full">
				<form action="" class="newsletter">
					<input type="text" placeholder="seu nome">
					<input type="email" placeholder="e-mail">
					<input type="submit" value="Enviar">
				</form>
			</div>
		</div>
		<!-- REDES SOCIAIS -->
		<div class="redes-sociais">
			<div class="row">
				<div class="col-sm-6">
					<a href="<?php echo $configuracao['config_site_link_facebook'] ?>" class="rede-social facebook"></a>
				</div>
				<div class="col-sm-6">
					<a href="<?php echo $configuracao['config_site_link_instagram'] ?>" class="rede-social instagram"></a>
				</div>
			</div>
		</div>
		<div class="container-full">
			<div class="row">
				<div class="col-sm-2">
					<!-- MINHA CONTA -->
					<div class="menu-footer minha-conta">
						<h4 class="titulo titulo-footer">Minha conta</h4>
						<nav>
							<a href="<?php echo esc_url( home_url( '/minha-conta/' ) ); ?>">Login</a>
							<a href="<?php echo esc_url( home_url( '/minha-conta/edit-account/' ) ); ?>">Meus dados</a>
							<a href="<?php echo esc_url( home_url( '/minha-conta/orders/' ) ); ?>">Meus pedidos</a>
							<a href="<?php echo esc_url( home_url( '/lista-de-desejos/' ) ); ?>">Lista de desejos</a>
						</nav>
					</div>
				</div>
				<div class="col-sm-2">
					<!-- INSTITUCIONAL -->
					<div class="menu-footer institucional">
						<h4 class="titulo titulo-footer">Institucional</h4>
						<nav>
							<a href="<?php echo esc_url( home_url( '/quem-somos/' ) ); ?>">Quem somos</a>
							<a href="<?php echo esc_url( home_url( '/como-comprar/' ) ); ?>">Como comprar</a>
							<a href="<?php echo esc_url( home_url( '/contato/' ) ); ?>">Contato</a>
						
							<a href="<?php echo esc_url( home_url( '/perguntas-frequentes/' ) ); ?>">Perguntas frequentes</a>
						</nav>
					</div>
				</div>
				<div class="col-sm-2">
					<!-- CATEGORIAS -->
					<div class="menu-footer categorias">
						<h4 class="titulo titulo-footer">Categorias</h4>
						<nav>
							<?php 
								foreach ($get_category_products as $get_category_menu):
									$verificacao	 =  in_array($get_category_menu->slug,$configuracao['header_links_menu']);
									if ($verificacao):
										$get_category_menu_name = $get_category_menu->name;
										$get_category_menu_link = get_category_link($get_category_menu->term_id);

							?>
							<a href="<?php echo $get_category_menu_link ?>"><?php echo $get_category_menu_name ?></a>
							<?php endif;endforeach; ?>
						</nav>
					</div>
				</div>
				<div class="col-sm-2">
					<!-- SELOS -->
					<div class="menu-footer selos">
						<h4 class="titulo titulo-footer">Selos</h4>
						
						<ul>
							<li>
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/selo1.png" alt="Selos">
									<figcaption class="hidden">Selo</figcaption>
								</figure>
							</li>
							<li>
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/selo2.png" alt="">
									<figcaption class="hidden">Selo</figcaption>
								</figure>
							</li>
							<li>
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/selo.png" alt="">
									<figcaption class="hidden">Selo</figcaption>
								</figure>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4">
					<!-- LOGO FOOTER -->
					<figure class="logo-footer">
						<img  src="<?php echo $configuracao['config_site_rodape_logo']['url'] ?> " alt="Logo Amor Sex Shop">
						<figcaption class="hidden">Logo Amor Sex Shop</figcaption>
					</figure>
				</div>
			</div>
		</div>
		<div class="pagamento-copyright">
			<div class="container-full">
				<div class="row">
					<div class="col-sm-6">
						<div class="formas-pagamento">
							<h5 class="titulo titulo-pagamento">Formas de pagamento</h5>
							<ul>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag1.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag2.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag3.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag4.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag5.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/pag6.png" alt="">
										<figcaption class="hidden">Forma de pagamento</figcaption>
									</figure>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="copyright">
							<p><?php echo $configuracao['config_site_rodape_copyright'] ?> <?php echo $configuracao['config_site_rodape_info'] ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>

