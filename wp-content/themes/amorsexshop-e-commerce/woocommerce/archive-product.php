<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

$category = get_queried_object();

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );


?>

<!-- PÁGINA DE CATEGORIAS -->
	<div class="pg pg-categorias">
		<div class="container-full">
			<div class="row">
				<div class="">
					<div class="col-sm-2">
						<!-- SIDEBAR -->
						<div class="sidebar">

						<?php
							include (TEMPLATEPATH . '/templates/sidebar_categoria.php');
						?>
					   </div>
					</div>
					<div class="col-sm-10">
						<div class="descricaoCategoria">
							<?php echo $category->description ?>
						</div>
						
						<div class="produto">
							<nav class="menu-caminho">
								<?php do_action( 'woocommerce_before_main_content' ); ?>
								<!-- <a href="#">Amor Sex Shop</a>
								<a href="#">Lingeries</a>
								<a href="#">Espartilhos</a> -->
							</nav>
							<div class="row">
								<div class="col-sm-6">
									
									<h2 class="titulo titulo-categoria"><?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) {woocommerce_page_title();} ?></h2>
								</div>
								<div class="col-sm-6">
									<div class="paginador-filtro">
										
										<div class="filtro">
											<?php woocommerce_catalog_ordering(); ?>
											<!-- <select name="Filtro">
												<option value="Mais vendido">Mais vendido</option>
												<option value="Menor preço">Menor preço</option>
												<option value="Maior preço">Maior preço</option>
											</select> -->
										</div>
									</div>
								</div>
							</div>
							
							<ul class="lista-produtos">
									<?php

									if ( wc_get_loop_prop( 'total' ) ) {
										while ( have_posts() ) {
											the_post();

											/**
											 * Hook: woocommerce_shop_loop.
											 *
											 * @hooked WC_Structured_Data::generate_product_data() - 10
											 */
											
											include (TEMPLATEPATH . '/templates/templates_spot/spot_produto _vitrine.php'); 
										}
									}
							
									
								?>
							</ul>
							<ul class="paginador">
								<?php woocommerce_pagination(); ?>
								<!-- <li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">Último</a></li> -->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer( 'shop' );

