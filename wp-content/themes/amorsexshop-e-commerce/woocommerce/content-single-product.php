<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

$productSku = $product->sku;
$minitaturas = $product->get_gallery_attachment_ids();
$minitaturasMobile = $product->get_gallery_attachment_ids();
$fotoProduto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoProduto = $fotoProduto[0];


/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<!-- PÁGINA PRODUTO -->
<div class="pg pg-produto">
	<div class="container-full">
		<div class="row">
				<div class="col-sm-6">
					<figure class="imagem-produto zoom" id='ex1'>
						<img src="<?php echo $fotoProduto ?>" alt="<?php echo get_the_title() ?>">
						<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
					</figure>
					<ul class="carrossel-produto">
						<?php 
							foreach ($minitaturas  as $miniatura ):
								$miniatura_url = wp_get_attachment_url( $miniatura );
						?>
							<li class="item-produto">
								<figure>
									<img src="<?php echo $miniatura_url  ?>" alt="<?php echo get_the_title() ?>" data-imagem="<?php echo $miniatura_url  ?>">
									<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
								</figure>
							</li>
						<?php endforeach; ?>					
					</ul>
				</div>
				<div class="col-sm-6">
					<div class="detalhes-produto">
						<nav class="menu-caminho">
							<?php do_action( 'woocommerce_before_main_content' ); ?>
						</nav>
						<span class="referencia">Referência do produto: <?php echo $productSku ?></span>
						<?php do_action( 'woocommerce_single_product_summary' ); 

						?>						
						<ul class="compartilhar">
							<li><a href="https://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="http://twitter.com/intent/tweet?text=<?php the_title();?>&url=<?php the_permalink();?>&via=seunomenotwitter" title="Twittar sobre <?php the_title();?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
							<li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" onclick="javascript:window.open(this.href,
                                '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                                 return false;" class="google"><i class="fab fa-google-plus-g"></i></a></li>
						</ul>
					</div>
				</div>
		</div>
		<div class="descricao">
			<h4 class="titulo titulo-descricao">Descrição do produto</h4>
			<div class="texto-descricao">
				<?php echo the_content() ?>
			</div>
		</div>
		
		<?php 
			global $product;
				
				$related = $product->get_related();
			    $relatedQtd = count($related);
			    $upsells = $product->get_upsells();
			    $upsellsQtd = count($upsells);
			    $meta_query = WC()->query->get_meta_query();
			    $args = array(
					'post_type'           => 'product',
					'ignore_sticky_posts' => 1,
					'no_found_rows'       => 1,
					'posts_per_page'      => $upsellsQtd,
					'orderby'             => 'rand',
					'post__in'            => $upsells,
					'post__not_in'        => array( $product->id ),
					'meta_query'          => $meta_query
			    );
		    	$loop = new WP_Query( $args );                
		if ($loop):

		?>
		<div class="relacionados">
				<div class="div-titulo">
					<h6 class="hidden">SEÇÃO DE PRODUTOS RELACIONADOS</h6>
					<h4 class="titulo">Relacionados</h4>
				</div>
				<ul class="carrossel carrossel-produtos lista-produtos">
					<?php 
						while($loop ->have_posts()): $loop ->the_post(); 
							//TEMPLATE SPOT CARROSSEL
							include (TEMPLATEPATH . '/templates/templates_spot/spot_produto _carrossel.php'); 
						endwhile; wp_reset_query();
					?>
					
				</ul>
		</div>
		<?php endif; ?>
	</div>
	<div class="comentariosProduto">
		<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
			    comments_template();
			endif;
	 	?>
	</div>
</div>

 <script type="text/javascript">
 	$("#ex1").zoom();
 </script>
