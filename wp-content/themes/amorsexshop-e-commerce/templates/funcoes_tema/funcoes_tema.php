<?php
/**
 * FUNÇÕES DO TEMA.
 * aDICIONES LOOPS E FUNÇÕES, PADRÕES PARA O TEMA
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amorsexshop-e-commerce
 */

/***************************************
		AVRIABLES PADRÕES
****************************************/
$get_category_product   = 'product_cat';


/***************************************
		FUNÇÃO GET CATEGORY
****************************************/
// GET PARENTS CATEGORIES
function get_category_products_woocomerce ($orderby, $hide_empty,$order,$parent){
   	
	$get_category_products = get_terms( $get_category_product, array(
		'orderby'       => $orderby,
		'hide_empty'    => $hide_empty,
		'order'         => $order,
		'parent'        => $parent,
	));

    return $get_category_products; 
}


