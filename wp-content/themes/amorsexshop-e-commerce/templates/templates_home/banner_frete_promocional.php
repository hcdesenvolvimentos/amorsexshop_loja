<!-- OPÇÕES/VANTAGENS DA LOJA -->
<div class="menu-opcoes-daloja">
	<ul class="opcoes">
		<li>
			<figure class="icon">
				<img src="<?php echo $configuracao['config_promo_inicial_banner_promocional_frete_icone_1']['url'] ?>" alt="Ícone">
			</figure>
			<span class="conteudo">
				<p><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_titulo_1'] ?></p>
				<span><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_desc_1'] ?></span>
			</span>
		</li>
		<li>
			<figure class="icon">
				<img src="<?php echo $configuracao['config_promo_inicial_banner_promocional_frete_icone_2']['url'] ?>" alt="Ícone">
			</figure>
			<span class="conteudo">
				<p><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_titulo_2'] ?></p>
				<span><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_desc_2'] ?></span>
			</span>
		</li>
		<li>
			<figure class="icon">
				<img src="<?php echo $configuracao['config_promo_inicial_banner_promocional_frete_icone_3']['url'] ?>" alt="Ícone">
			</figure>
			<span class="conteudo">
				<p><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_titulo_3'] ?></p>
				<span><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_desc_3'] ?></span>
			</span>
		</li>
		<li>
			<figure class="icon">
				<img src="<?php echo $configuracao['config_promo_inicial_banner_promocional_frete_icone_4']['url'] ?>" alt="Ícone">
			</figure>
			<span class="conteudo">
				<p><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_titulo_4'] ?></p>
				<span><?php echo $configuracao['config_promo_inicial_banner_promocional_frete_desc_4'] ?></span>
			</span>
		</li>	
	</ul>
</div>

