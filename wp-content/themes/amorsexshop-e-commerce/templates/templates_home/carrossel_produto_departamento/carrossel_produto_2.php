<?php 
	$tituloSessao  = $configuracao["inicial_sessao_carrossel_departamento_titulo_sessao_2"];
	$tituloBanner  = $configuracao["inicial_sessao_carrossel_departamento_titulo_banner_2"];
	$slugBannerCategoria = $configuracao["inicial_sessao_carrossel_departamento_slug_2"];
	$linkBannerCategoria = $configuracao['inicial_sessao_carrossel_departamento_banner_link_2'];
	$imagemBannerCategoria =  $configuracao['inicial_sessao_carrossel_departamento_banner_2']['url'];
	
	$produtosCarrossel_2 = new WP_Query(array(
		'post_type'     => 'product',
		'posts_per_page'   => 10,
		'order' => 'rand',
		'tax_query'     => array(
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $slugBannerCategoria ,
				)
			)
		)
	);
	if ($slugBannerCategoria):
?>
<!-- SEÇÃO CARROSSEL PRODUTOS -->
	<div class="div-titulo">
		<h2 class="titulo titulo-categoria-produtos-inicial"><?php  echo $tituloSessao ?></h2>
	</div>
	<ul class="carrossel carrossel-produtos lista-produtos">
		<?php 
			// LOOP DE POST
			while ( $produtosCarrossel_2->have_posts() ) : $produtosCarrossel_2->the_post();
				//TEMPLATE SPOT CARROSSEL
				include (TEMPLATEPATH . '/templates/templates_spot/spot_produto _carrossel.php');
			endwhile; wp_reset_query(); 
		?>	
		<li class="item-produto confira">
			<a href="<?php echo $linkBannerCategoria ?>">
				<figure>
					<img src="<?php echo $imagemBannerCategoria ?>" alt="<?php echo $imagemBannerCategoria ?>">
					<figcaption class="hidden"><?php echo $imagemBannerCategoria ?></figcaption>
				</figure>
					<p><?php  echo $tituloBanner ?></p>
			</a>
		</li>			
	</ul>

<?php endif; ?>