<div class="marcas">
	<ul class="carrossel carrossel-marcas">
	<?php 
	$categoria = 'product_cat';

	 $categoriasProdutos = get_terms( $categoria, array(
			'orderby'       => 'count',
			'hide_empty'    => true,
			'parent'	    => 21,
			'order'         => 'DESC',
		));
      if(!empty($categoriasProdutos)) :
      
		foreach( $categoriasProdutos as $categoriaProduto ):
			
			// GET RETORNA O ID DA IMAGEM
			$thumbnail_id = get_woocommerce_term_meta($categoriaProduto->term_id, 'thumbnail_id', true);
			// GET RETORNA A URL DA IMAGEM ATRAVÉS DO ID
			$urlImage = wp_get_attachment_url($thumbnail_id);
			// GET RETORNA O LINK DA CATEGORIA ATRAVÉS DO ID
			$link_category_marca = get_category_link($categoriaProduto->term_id);
			$name_category_marca = get_category_link($categoriaProduto->name);
	     ?>
	     	
		<li><a href="<?php echo $link_category_marca ?>">
				<figure>
					<img src="<?php echo $urlImage ?>" alt="Imagem da marca<?php echo $name_category_marca->name ?>">
					<figcaption class="hidden">Marca</figcaption>
				</figure>
			</a>
		</li>	
		<?php endforeach; 
	 endif; wp_reset_query();  ?>					
						
	</ul>
</div>