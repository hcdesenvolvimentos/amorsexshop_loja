
<!-- CARROSSEL DE DESTAQUE -->

<div class="carrossel-destaque">
	<?php 
			//LOOP CARROSSEL DESTAQUE
			$destaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
			while ( $destaques->have_posts() ) : $destaques->the_post();
				$destaque_link = rwmb_meta('AmoreSexShop_destaque_link');
		 ?>
		<!-- ITEM -->
		<?php if ($destaque_link != ''): ?>			
		
			<a href="<?php echo $destaque_link ?>" class="item" style="background: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);">
				<figure>
					<img class="img-responsive" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden">Destaque</figcaption>
				</figure>
			</a>
		<?php else :  ?>
		    <a  class="item" style="background: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);">
				<figure>
					<img class="img-responsive" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden">Destaque</figcaption>
				</figure>
			</a>
		<?php endif; endwhile; wp_reset_query(); ?>

</div>