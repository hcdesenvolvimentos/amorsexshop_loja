<?php 
    // INFO BANNER 1 
    $imagemBannerBanner1  = $configuracao["config_site_inicial_mini_banner_1"]['url'];
	$tituloBanner1  = $configuracao["config_site_inicial_mini_banner_titulo_1"];
	$subtituloBanner1 = $configuracao["config_site_inicial_mini_banner_subtitulo_1"];
	$linkBanner1 = $configuracao['config_site_inicial_mini_banner_link_1'];    
	
	// INFO BANNER 2 
    $imagemBannerBanner2  = $configuracao["config_site_inicial_mini_banner_2"]['url'];
	$tituloBanner2  = $configuracao["config_site_inicial_mini_banner_titulo_2"];
	$subtituloBanner2 = $configuracao["config_site_inicial_mini_banner_subtitulo_2"];
	$linkBanner2 = $configuracao['config_site_inicial_mini_banner_link_2'];  


	// INFO BANNER 3
    $imagemBannerBanner3  = $configuracao["config_site_inicial_mini_banner_3"] ['url'];
	$tituloBanner3  = $configuracao["config_site_inicial_mini_banner_titulo_3"];
	$subtituloBanner3 = $configuracao["config_site_inicial_mini_banner_subtitulo_3"];
	$linkBanner3 = $configuracao['config_site_inicial_mini_banner_link_3'];
?>

<!-- MINI DESTAQUES -->
<div class="destaques-menor">
	<a href="<?php echo $linkBanner1 ?>"	 class="destaque">
		<figure class="destaque-imagem">
			<img src="<?php echo $imagemBannerBanner1?>" alt="Imagem do banner <?php  echo $tituloBanner1 ?>">
			<figcaption class="hidden">Destaque <?php  echo $tituloBanner1 ?> </figcaption>
		</figure>
		<div class="destaque-conteudo">
			<h3><?php  echo $tituloBanner1 ?></h3>
			<span><?php  echo $subtituloBanner1 ?></span>
		</div>
	</a>
	<a href="<?php echo $linkBanner2 ?>"	 class="destaque">
		<figure class="destaque-imagem">
			<img src="<?php echo $imagemBannerBanner2 ?>" alt="Imagem do banner <?php  echo $tituloBanner2 ?>">
			<figcaption class="hidden">Destaque <?php  echo $tituloBanner2 ?> </figcaption>
		</figure>
		<div class="destaque-conteudo">
			<h3><?php  echo $tituloBanner2 ?></h3>
			<span><?php  echo $subtituloBanner2 ?></span>
		</div>
	</a>
	<a href="<?php echo $linkBanner3 ?>"	 class="destaque">
		<figure class="destaque-imagem">
			<img src="<?php echo $imagemBannerBanner3 ?>" alt="Imagem do banner <?php  echo $tituloBanner3 ?>">
			<figcaption class="hidden">Destaque <?php  echo $tituloBanner3 ?> </figcaption>
		</figure>
		<div class="destaque-conteudo">
			<h3><?php  echo $tituloBanner3 ?></h3>
			<span><?php  echo $subtituloBanner3 ?></span>
		</div>
	</a>

</div>