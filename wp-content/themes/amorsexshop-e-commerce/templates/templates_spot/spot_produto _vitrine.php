<?php 
	//VARIÁVEL GLOBAL DO PRODUTO 
	global $product;
	//FOTO PRODUTO
	$fotoProduto      = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoProduto      = $fotoProduto[0];
	// FOTO ZOOM 
	$fotoProduto_zoom = rwmb_meta('AmoreSexShop_produto_zoom');
	//PREÇO NROMAL
	$price            = str_replace('.', ',', $product->price);
	// PREÇO REGULAR
	$regular_price    = str_replace('.', ',', $product->regular_price);
	//PREÇO PROMOCIONAL
	$sale_price       = str_replace('.', ',', $product->sale_price);
	// //FOREACH PARA PEGAR URL DA FOTO
	foreach ($fotoProduto_zoom as $fotoProduto_zoom) { 
		$fotoProduto_zoom =  $fotoProduto_zoom['full_url'];
	}
	if ($fotoProduto_zoom) {
		$fotoProduto_zoom =  $fotoProduto_zoom;
	}else{
		$fotoProduto_zoom =  $fotoProduto;
	}
?>

<li class="item-produto">
	<span  data-add_to_wishlist="<?php echo $product->id ?>" class="add-lista-desejos"><img src="<?php echo get_template_directory_uri(); ?>/img/desire.svg" alt=""></span>
	<a href="<?php echo get_permalink() ?>" class="imagem-produto">
		<figure>
			<img src="<?php echo $fotoProduto ?>" data-hover="<?php echo $fotoProduto_zoom ?>" data-inicial="<?php echo $fotoProduto ?>" alt="<?php echo $product->name ?>"  data-img-zoom="<?php echo $fotoProduto_zoom ?>">
			<figcaption class="hidden"><?php echo $product->name ?></figcaption>
		</figure>
	</a>
	<div class="info-produto">
		<h3 class="titulo nome-produto"><?php echo $product->name ?></h3>
	<div class="preco-produto">

		<?php if ($sale_price):?>
		<small>
			<del>R$<?php echo $regular_price ?></del>
		</small>
		<p class="preco">R$<?php echo $sale_price ?></p>
		<p class="parcela">
			<?php 
				$calculoParcelas     = $sale_price/5;  
				$precoParcela_format = round($calculoParcelas, 2);
				$precoParcela        = str_replace('.', ',', $precoParcela_format);
				if ($precoParcela>10) {
					echo "5x de<br>";
					echo "R$".$precoParcela;
				}
			?>
		</p>	     

		<?php else: if ($price):?>
		<p class="preco">R$<?php echo $price ?></p>
		<p class="parcela">
			<?php 
					$calculoParcelas_price     = $price/5;  
					$precoParcela_format_price = round($calculoParcelas_price, 2);
					$precoParcela_price        = str_replace('.', ',', $precoParcela_format_price);
					if ($precoParcela_price>10) {
						echo "5x de<br>";
						echo "R$".$precoParcela_price;
					}
					
				?></p>
		<?php endif;endif; ?>
	</div>
		<div class="button-comprar">
			<a  href="<?php echo $current_url = home_url( add_query_arg( array(), $wp->request ) ) ?>/?add-to-cart=<?php echo $product->id ?>" data-quantity="1" class="button btnAddCart product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo $product->id ?>" data-product_sku="" aria-label="Adicionar “Produto 4” no seu carrinho" rel="nofollow">Comprar</a>
		</div>
	</div>
</li>