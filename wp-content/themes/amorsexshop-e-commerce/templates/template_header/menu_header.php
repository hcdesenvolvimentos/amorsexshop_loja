<?php
/**
 * MENU HEADER
 *
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amorsexshop-e-commerce
 */
// TEMPLATES FUNÇÕES TEMA - FUNÇÕES DO TEMA
include (TEMPLATEPATH . '/templates/funcoes_tema/funcoes_tema.php');
$get_categoryes_menu = get_category_products_woocomerce("count", "true","DESC","0");
?>
<div class="container-full">
	<!-- MENU PRINCIPAL -->
	<nav class="menu-principal">
		<ul>
			<?php 
				foreach ($get_categoryes_menu as $get_category_menu):
					$verificacao	 =  in_array($get_category_menu->slug,$configuracao['header_links_menu']);
					if ($verificacao):
						
						$get_category_menu_name = $get_category_menu->name;
						$get_category_menu_link = get_category_link($get_category_menu->term_id);
						
						if (z_taxonomy_image_url($get_category_menu->term_id, 'medium')){
							$get_category_menu_img = z_taxonomy_image_url($get_category_menu->term_id, 'medium');
						}else{
							$get_category_menu_img = get_template_directory_uri()."/img/exemple.svg";
						}


			?>
			<li class="menu-principal-item">
				<img src="<?php echo $get_category_menu_img ?>" alt="<?php  echo $get_category_menu_name ?>">
				<a href="<?php echo $get_category_menu_link ?>"><?php echo $get_category_menu_name ?></a>

				<!-- TEMPLATE SUB MENU -->
				<?php include (TEMPLATEPATH . '/templates/template_header/sub_menu_header.php'); ?>
			</li>
			<?php endif;endforeach; ?>
		</ul>
	</nav>
</div>