<?php

/**
 * Funções E-commerce
 *
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amorsexshop-e-commerce
 */

global $configuracao;
global $woocommerce;
global $wp;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
$cart_subtotal = $woocommerce->cart->get_cart_subtotal();

// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' ';
if (!$qtdItensCarrinhoRotulo != 0){
	$qtdItensCarrinhoRotulo = "0";
}


$totalCarrinho = str_replace('.', ',', WC()->cart->cart_contents_total);

if ($totalCarrinho != 0) {
	$totalCarrinho = $totalCarrinho ;
}else{
	$totalCarrinho = "00,00";
}
 //wc_print_notices()
