
<?php 
	
	//MONTANDO ARRAY DE SUB CATEGORIAS
	$get_category_children_products = get_terms( $get_category_product, array(
		'orderby'       => $orderby,
		'hide_empty'    => $hide_empty,
		'order'         => $order,
		//ID CATEGORIA PAI - LOOP ANTERIOR
		'parent'        => $get_category_menu->term_id,
	));

	//VERIFICANDO ARRAY VAZIO/CASO FALSE MONTA O HTML
	if (!empty($get_category_children_products)):
?>
<div class="div-submenu">
	<ul class="submenu">
		<?php foreach ($get_category_children_products  as $get_category_children_product): ?>
		<li class="item-submenu">
			<a href="<?php echo get_category_link($get_category_children_product->term_id) ?>"><?php echo $get_category_children_product->name; ?></a>
		</li>
		<?php endforeach ?>
	</ul>

	<?php if (trim(get_term_meta( $get_category_menu->term_id, 'img_cat_link', true )) !=''):?>
	<div class="anuncio">
		<figure>
			<img src="<?php echo get_term_meta( $get_category_menu->term_id, 'img_cat_menu', true );?>" alt="<?php echo get_term_meta( $get_category_menu->term_id, 'img_cat_menu', true );?>">
			<figcaption class="hidden"><?php echo get_term_meta( $get_category_menu->term_id, 'img_cat_menu', true );?></figcaption>
		</figure>
		<div class="button-confira">
			<a href="<?php echo get_term_meta( $get_category_menu->term_id, 'img_cat_link', true );?>">Confira!</a>
		</div>
	</div>
	<?php endif; ?>
</div>
<?php endif; ?>