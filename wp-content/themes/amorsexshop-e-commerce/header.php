<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amorsexshop-e-commerce
 */
// TEMPLATES FUNÇÕES HEADER - WOOCOMERCE
include (TEMPLATEPATH . '/templates/template_header/funcoes_woocomerce.php');
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
	
	<div class="abrir-menu-mobile">
		<span class="um"></span>
		<span class="dois"></span>
		<span class="tres"></span>
	</div>

	<div class="container-full">
		<!-- MENU DO TOPO -->
		<div class="menu-topo">
			<div class="row">
				<div class="col-sm-6">
					<p><?php echo $configuracao['header_frase'] ?> <a href="<?php echo esc_url( home_url( '/minha-conta/' ) ); ?>">cadastro</a>!</p>
				</div>
				<div class="col-sm-6">
					<ul>
						<?php 
						foreach ( $configuracao['header_links'] as  $configuracao['header_links']):
							$itemFormatado = 	explode("|", $configuracao['header_links']);
						?>
						<li> 
							<a href="<?php echo $itemFormatado[1] ?>" title="<?php echo $itemFormatado[0] ?>"><?php echo $itemFormatado[0] ?></a> 
						</li>
					   <?php endforeach; ?>							
						<li class="icon"><a href="<?php echo $configuracao['config_site_link_facebook'] ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.svg" alt="icone-facebook"></a></li>
						<li class="icon"><a href="<?php echo $configuracao['config_site_link_instagram'] ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg" alt="icone-instagram"></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- MENU DO MEIO -->
	<div class="menu-meio">
		<div class="container-full">
			<div class="row">
			
				<div class="col-sm-2">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>" class="logo">
						<figure>
							<img class="img-responsive" src="<?php echo $configuracao['header_logo']['url'] ?> " alt="<?php bloginfo( 'name' ); ?>">
							<figcaption class="hidden"><?php bloginfo( 'name' ); ?></figcaption>
						</figure>
					</a>
				</div>
				
				<div class="col-sm-6">
					<div class="login-pesquisa">
						<?php if ($current_user->display_name):?>
						<div class="menu-login">
							<p class="login">Olá <a href="<?php echo esc_url( home_url( '/minha-conta/' ) ); ?>"><strong><?php echo $current_user->display_name ?></strong></a></p>
							<p class="whatsapp"><a href="tel:<?php echo $configuracao['header_telefone'] ?>" title="<?php echo $configuracao['header_telefone'] ?>"> <?php echo $configuracao['header_telefone'] ?></a></p>
						</div>
						<?php else: ?>
						<div class="menu-login">
							<p class="login">Faça <a href="<?php echo esc_url( home_url( '/minha-conta/' ) ); ?>"><strong>login</strong></a> ou <a href="<?php echo esc_url( home_url( '/minha-conta/' ) ); ?>"><strong>cadastra-se</strong></a></p>
							<p class="whatsapp"><a href="tel:<?php echo $configuracao['header_telefone'] ?>" title="<?php echo $configuracao['header_telefone'] ?>"> <?php echo $configuracao['header_telefone'] ?></a></p>
						</div>
						<?php endif; ?>
						<div class="pesquisa">
							<?php echo do_shortcode('[wcas-search-form]'); ?>
						</div>
					</div>
				</div>
			
				<div class="col-sm-4">
					<div class="desejos-carrinho">
						<div class="row">
							<div class="col-sm-6">
								<a href="<?php echo esc_url( home_url( '/lista-de-desejos/' ) ); ?>" class="meus-produtos lista-de-desejos">
									<figure class="icon">
										<img src="<?php echo get_template_directory_uri(); ?>/img/heart.svg" alt="Lista de desejos">
									</figure>
									<div class="conteudo">
										<p><strong>Lista de desejos</strong></p>
										<!-- <span>Minha Lista</span> -->
									</div>
								</a>
							</div>
							<div class="col-sm-6">
								<a  href="<?php echo $urlCarrinho  ?>" title="Meu Carrinho" class="meus-produtos carrinho">
									<figure class="icon">
										<img src="<?php echo get_template_directory_uri(); ?>/img/cart.svg" alt="Carrinho">
									</figure>
									<div class="conteudo">
										<p><?php echo $qtdItensCarrinhoRotulo; ?>itens no carrinho</p>											
										<span id="preco">R$<?php echo $totalCarrinho ?></span>
									</div>
								</a>
								<div class="carrinhoMinicart">
									<div id="mini-cart-loja">
										<?php woocommerce_mini_cart(); ?>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

<!-- TEMPLATES FUNÇÕES HEADER - MENU -->
<?php include (TEMPLATEPATH . '/templates/template_header/menu_header.php'); ?>

</header>

